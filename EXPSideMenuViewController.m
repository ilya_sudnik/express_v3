//
//  EXPSideMenuViewController.m
//  Express_v3
//
//  Created by Ilia Sudnik on 14.08.14.
//  Copyright (c) 2014 GPShopper. All rights reserved.
//

#import "EXPSideMenuViewController.h"
#import "TWTSideMenuViewController.h"
#import "EXPHomeViewController.h"
#import "EXPSideMenuViewController.h"
#import "EXPStoresViewController.h"
#import "EXPAppDelegate.h"

@interface EXPSideMenuViewController ()


@end

@implementation EXPSideMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor blackColor];
    self.tableView.backgroundColor = [UIColor clearColor];

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITableViewDelegate & UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _viewControllers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reusableIdentifier = @"sideMenuTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"sideMenuTableCell"];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
    }
    
	UIViewController *vc = self.viewControllers[indexPath.row];
	
    cell.textLabel.text = vc.title;
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIViewController *vc = _viewControllers[indexPath.row];
    if (self.sideMenuViewController.mainViewController == vc.navigationController)
    {
        [self.sideMenuViewController closeMenuAnimated:YES
                                            completion:nil];
        return;
    }
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    navController.navigationBar.translucent = NO;
	
    [self.sideMenuViewController setMainViewController:navController
                                              animated:YES
                                             closeMenu:YES];  
}


@end
