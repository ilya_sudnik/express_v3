//
//  EXPViewController.m
//  Express_v3
//
//  Created by Ilia Sudnik on 15.08.14.
//  Copyright (c) 2014 GPShopper. All rights reserved.
//

#import "EXPViewController.h"

@interface EXPViewController ()

@end

@implementation EXPViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        UIBarButtonItem *sideMenuButton = [[UIBarButtonItem alloc] initWithTitle:@"Menu"
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(openButtonPressed)];
        sideMenuButton.tintColor = [UIColor blackColor];
        [self.navigationItem setLeftBarButtonItem:sideMenuButton];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openButtonPressed
{
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}

@end
