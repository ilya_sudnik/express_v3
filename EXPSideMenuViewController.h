//
//  EXPSideMenuViewController.h
//  Express_v3
//
//  Created by Ilia Sudnik on 14.08.14.
//  Copyright (c) 2014 GPShopper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TWTSideMenuViewController.h"

@interface EXPSideMenuViewController : UIViewController  <UITableViewDataSource, UITableViewDelegate, TWTSideMenuViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UITableView* tableView;
@property (nonatomic, strong) NSArray* viewControllers;


@end
