//
//  EXPAppDelegate.h
//  Express_v3
//
//  Created by Andrew Danielyan on 8/13/14.
//  Copyright (c) 2014 GPShopper. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TWTSideMenuViewController.h"
#import "EXPSideMenuViewController.h"
#import "EXPHomeViewController.h"
#import "EXPStoresViewController.h"

@interface EXPAppDelegate : UIResponder <UIApplicationDelegate, TWTSideMenuViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) TWTSideMenuViewController *sideMenuViewController;
@property (nonatomic, strong) EXPSideMenuViewController *menuViewController;
@property (nonatomic, strong) EXPHomeViewController *homeViewController;
@property (nonatomic, strong) EXPStoresViewController *storesViewController;





@end
