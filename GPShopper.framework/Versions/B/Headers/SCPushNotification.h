//
//  SCPushNotification.h
//  bebe
//
//  Created by donny on 9/11/12.
//
//

#import <Foundation/Foundation.h>
#import "Banner.h"
#import "BannerView.h"
#import "SCWebTracking.h"

@protocol PushNotificationDelegate < BannerActionDelegate >
-(void)pushNotificationGotoHomePage;
@end


@interface SCPushNotification : NSObject < SCBanner >

@property (readonly) NSString *messageId;
@property (readonly) NSString *message;
@property (readonly) enum SCBannerType bannerType;
@property (readonly) NSString *scriptType;
@property (readonly) NSString *scriptValue;

@property (readwrite) uint64_t ggid;
@property (readwrite) uint64_t grpid;
@property (copy) NSString *url;
@property (copy) NSString *searchQuery;
@property (copy) NSString *sectionName;
@property (readwrite) uint64_t contestid;
@property (readwrite) uint64_t eventid;

-(id)initWithMessageId: (NSString *)mi
               message: (NSString *)m
            scriptType: (NSString *)st
           scriptValue: (NSString *)sv;

-(SCWebTrackingEvent *)trackingEvent;

-(BOOL)actionDataIsValid;

-(void)performActionWithDelegate: (id<PushNotificationDelegate>)d;

+(SCPushNotification *)newFromDictionary: (NSDictionary *)d;

+(SCPushNotification *)defaultPushNotification;

@end


