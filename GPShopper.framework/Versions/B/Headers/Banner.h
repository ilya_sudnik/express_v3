//
//  Banner.h
//  Slifter_iphone_rewrite
//
//  Created by kimberly on 1/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//


#ifndef BANNER_H
#define BANNER_H

//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


enum SCBannerType
{
    SCBannerUnknown,
	SCBannerGiftGuide,
	SCBannerProduct,
	SCBannerUrl,
	SCBannerQuattro,
	SCBannerEmail,
	SCBannerMultimediaTemplate,
	SCBannerSearch,
	SCBannerSection,
	SCBannerContest,
    SCBannerPromo,
    SCBannerEvent,
    SCBannerCustom
};


//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


@protocol SCBanner < NSObject >

-(enum SCBannerType)bannerType;
-(uint64_t)ggid;
-(uint64_t)grpid;
-(NSString *)url;
-(NSString *)searchQuery;
-(NSString *)sectionName;
-(NSString *)scriptValue;
-(uint64_t)contestid;
-(uint64_t)eventid;

@end

#define BANNER_DATE_FORMAT @"yyyy-MM-dd HH:mm:ss"

@interface Banner : NSObject < SCBanner >
{
	NSMutableDictionary *contents;
    
}

@property (readonly) enum SCBannerType bannerType;
@property (readonly) uint64_t ggid;
@property (readonly) uint64_t grpid;
@property (readonly) NSString *url;
@property (readonly) NSString *searchQuery;
@property (readonly) NSString *sectionName;
@property (readonly) NSString *scriptValue;
@property (readonly) uint64_t contestid;
@property (readonly) uint64_t eventid;
@property (readonly) NSDate *endDate;

-(id)initWithDict: (NSDictionary *)dict;

-(id)initWithBannerid: (uint64_t)bid
                 type: (enum SCBannerType)t
         positionName: (NSString *)pn
              endDate: (NSDate *)d
          scriptValue: (NSString *)sv;

-(NSString *)positionName;
-(uint64_t)bannerID;
-(NSString *)bannerName;
-(NSDictionary *)attributes;
-(NSString *)md5;
-(void)setMd5: (NSString *)m;

+(NSString *)nameForType: (enum SCBannerType)t;
+(NSString *)scriptValueFromBanner: (id<SCBanner>)b;
+(Banner *)newFromDictionary: (NSDictionary *)kv;

@end


//-------------------------------------------------------------------------------
//-------------------------------------------------------------------------------


@class SCCache;

#define kDefaultBannerManagerTimeout 600

@interface BannerManager : NSObject
{}

+(void)setRequestUrl: (NSString *)url;
+(void)setImageCachingEnabled: (BOOL)ice;

/**
 *  Defines how long before Banners are considered 'stale' and need to be refetched/refreshed.
 *  Default value is kDefaultBannerManagerTimeout.
 *
 *  @param seconds Number of seconds after which Banner is considered 'stale'
 */
+(void)setDataTimeout: (double)seconds;

+(void)setDataTimeoutEnabled: (BOOL)enabled;

+(NSArray *)bannersForPositionName: (NSString *)n;

+(Banner *)bannerForID: (uint64_t)bannerid;
+(Banner *)randomBannerForName: (NSString *)n;
+(Banner *)nthBanner: (NSUInteger)i
			 forName: (NSString *)n;
+(NSUInteger)numBannersForName: (NSString *)n;
+(uint64_t)numBanners;
+(void)refetchAll;
+(void)refetchBannersForName: (NSString *)n;

+(SCCache *)cache;

@end

#endif
