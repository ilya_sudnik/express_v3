//
//  EXPStoresViewController.m
//  Express_v3
//
//  Created by Ilia Sudnik on 14.08.14.
//  Copyright (c) 2014 GPShopper. All rights reserved.
//

#import "EXPStoresViewController.h"

@interface EXPStoresViewController ()

@end

@implementation EXPStoresViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.title = @"Stores";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = [UIColor redColor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)openButtonPressed
{
    [self.sideMenuViewController openMenuAnimated:YES completion:nil];
}


@end
