//
//  main.m
//  Express_v3
//
//  Created by Andrew Danielyan on 8/13/14.
//  Copyright (c) 2014 GPShopper. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EXPAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([EXPAppDelegate class]));
	}
}
