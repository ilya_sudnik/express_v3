//
//  EXPHomeViewController.m
//  Express_v3
//
//  Created by Ilia on 14.08.14.
//  Copyright (c) 2014 GPShopper. All rights reserved.
//

#import "EXPHomeViewController.h"
#import "EXPAppDelegate.h"

@interface EXPHomeViewController ()

@end

@implementation EXPHomeViewController

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"EXPRESS";
    self.view.backgroundColor = [UIColor redColor];
    

}

@end
